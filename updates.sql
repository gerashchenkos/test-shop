//02-11-2019
ALTER TABLE `users` MODIFY `password` VARCHAR(50);

//03-11-2019
CREATE INDEX parent_id ON categories(parent_id);

//09-11-2019
ALTER TABLE `users` ADD COLUMN `type` VARCHAR(50) DEFAULT 'customer';