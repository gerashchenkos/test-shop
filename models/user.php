<?php
    
    function checkLogin($db, $salt, $email, $pass)
    {
        $pass = sha1($pass.$salt);
        $stmt = $db->prepare("SELECT * FROM users WHERE email = :email and password = :password");
        $stmt->execute(["email" => $email, "password" => $pass]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if(!empty($result)){
            return $result['id'];
        } else{
            return 0;
        }
    }
?>