<?php
namespace Shop;

class Admin extends User{

    public function __construct()
    {
    }

    public function save()
    {
    }

    public function changePassword($user, $pass)
    {
        $pass = $user->encryptPass($pass);
        $stmt = DB::$conn->prepare("UPDATE users SET password = :password WHERE id = :id");
        $stmt->execute(["password" => $pass, "id" => $user->id]);
        return true;
    }


}