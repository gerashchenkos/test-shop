<?php
namespace Shop;

class Category{
    public $id;
    public $title;
    public $parentId;
    protected $createdDate;
    protected $updatedDate;

    public function __construct($title = null, $parentId)
    {
        $this->title = $title;
        $this->parentId = $parentId;
    }

    public function save()
    {
        $stmt = DB::$conn->prepare("INSERT INTO categories(`title`, `parent_id`) VALUES(:title, :parent_id)");
        $stmt->execute(['title' => $this->title, 'parent_id' => $this->parentId]);
        $this->id = DB::$conn->lastInsertId();
        return $this->id;
    }

    public static function getCategories()
    {
        $stmt = DB::$conn->query("SELECT * FROM categories");
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function fetchCategoryTreeList($parent = 0, $userTreeArray = '')
    {
        if (!is_array($userTreeArray)){
            $userTreeArray = [];
        }
        $sql = "SELECT * FROM `categories` WHERE `parent_id` = $parent ORDER BY id ASC";
        $stmt = DB::$conn->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
        if (count($result) > 0)
        {
            $userTreeArray[] = "<ul>";
            foreach($result as $row){
                $userTreeArray[] = "<li><a href='/index.php?category=".$row['id']."'>". $row["title"]."</a></li>";
                $userTreeArray = self::fetchCategoryTreeList($row["id"], $userTreeArray);
            }
            $userTreeArray[] = "</ul><br/>";
        }
        return $userTreeArray;
    }

}